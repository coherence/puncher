=====================================================
Puncher - an InternetGatewayDevice manipulation tool
=====================================================

Puncher is a command-line tool to work with InternetGatewayDevices. An
UPnP InternetGatewayDevice is a subsystem, found probably in every
present router, that allows to query informations about the WAN
connection, the link status, ones external IP address, and last, but
not least, to enable port-mappings that allow connections from the
Internet through the firewall to an application in the local LAN.

Obviously this should only be enabled in a very, very trusted
environment, but it can't be dismissed that it can be quite useful
under certain circumstances.

Puncher accepts these options and commands:

:--timeout= | -t: the time in seconds Puncher should wait for an
   InternetGatewayDevice to show up, the default is 10 seconds. Do to
   the nature of UPnP we first have to search for a device and when
   found we can establish a (control) connection to it.

:--uuid= | -u'': the UUID of the device Puncher should interact with.
   In case there is more than one IGD in the local LAN. If not
   specified Puncher will work with the first device that shows up.

  ''show-devices:'':: will list all InternetGatewayDevices found
  within the given timeout period.

  ''info:'':: print details like the external IP address, the number
  of port-mappings.

  ''show-mappings:'':: list all UPnP created port-mappings of an
  InternetGatewayDevice.

  ''add-mapping:'':: add a port-mapping to an InternetGatewayDevice[[BR]]

     * ''--remote-host='': remote host address (empty for any) [default: ""]
     * ''--external-port='': external-port [default: identical to the internal-port]
     * ''--internal-host= | -h'': internal host address
     * ''--internal-port= | -p'': internal port
     * ''--protocol='': tcp or udp [default: tcp]
     * ''--active='': active, yes or no [default: y]
     * ''--lease-duration='': duration of that mapping [default: 0]
     * ''--description='': description [default: none][[BR]]

  ''delete-mapping:'':: remove a port-mapping from an InternetGatewayDevice

     * ''--remote-host= | -h'': remote host address (empty for any) [default: ""]
     * ''--external-port=| -p'': external-port
     * ''--protocol='': tcp or udp [default: tcp]

Examples
============

::

 $ puncher show-devices
 1 InternetGatewayDevice found:
 nitrox UPnP/1.0 AVM FRITZ!Box Fon WLAN 7270 54.04.80 with uuid:75802409-xywz-4711-0815-001122334455

::

 $ puncher info
 1 InternetGatewayDevice found:
 nitrox UPnP/1.0 AVM FRITZ!Box Fon WLAN 7270 54.04.80 with uuid:75802409-xywz-4711-0815-001122334455
 External IP address:  74.125.43.99
 Number of port-mappings:  0

::

 $ puncher add-mapping -h 192.168.123.123 -p 12345 --active=no --description="Test from Puncher"
 port-mapping to nitrox UPnP/1.0 AVM FRITZ!Box Fon WLAN 7270 54.04.80 added

::

 $ puncher show-mappings
 1 InternetGatewayDevice found:
 nitrox UPnP/1.0 AVM FRITZ!Box Fon WLAN 7270 54.04.80 with uuid:75802409-xywz-4711-0815-001122334455
 Ext. Port | Remote Host    | Int. Port | Internal Host   | Prot. | active | duration | description
 ====================================================================================================
     12345 | any            |     12345 | 192.168.123.123 |   TCP |     no | infinite | Test.from.Puncher
 ====================================================================================================

::

 $ puncher delete-mapping  -p 12345
 port-mapping deleted from nitrox UPnP/1.0 AVM FRITZ!Box Fon WLAN 7270 54.04.80


Download and Installation
==========================

Requirements:
* Coherence
* Twisted


Licence
================

Puncher is licensed under the
[http://www.opensource.org/licenses/mit-license.php MIT license].

Dependencies
==============
 * Python >= 2.6
 * [http://coherence-projects.org Coherence]
